const mongoose = require('mongoose')
const Schema = mongoose.Schema

const UserSkills = new Schema(
    {
        skillsName: { type: String, required: true },
        skillsCategory: { type: Number, required: true },
        skillsRate: { type: Number, required: true },
        userId: { type: String, required: true },
        status: { type: Number, required: true },
    },
    { timestamps: true },
)

module.exports = mongoose.model('userSkills', UserSkills)