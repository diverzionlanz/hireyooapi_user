const mongoose = require('mongoose')
const Schema = mongoose.Schema

const Campaign = new Schema(
    {
        campaignName: { type: String, required: true },
        status: { type: Number, default: 0 },
        userId: { type: String, required: true },
        
    },
    { timestamps: true },
)

module.exports = mongoose.model('campaign', Campaign)