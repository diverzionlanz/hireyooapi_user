const mongoose = require('mongoose')
const Schema = mongoose.Schema

const UserApplication = new Schema(
    {
        mediaId: { type: String, required: true },
        userId: { type: String, required: true },
        status:{ type: String, required: true },
        jobId:{ type: String, required: true },
        messageId: { type: String, required: true },
        
    },
    { timestamps: true },
)

module.exports = mongoose.model('userApplication', UserApplication)