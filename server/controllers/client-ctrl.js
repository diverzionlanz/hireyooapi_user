const Client = require('../models/client-model')
const ClientAddress = require('../models/client-address-model')
const bcrypt = require("bcrypt");
const mongoose = require('mongoose')
require('dotenv').config()

createClient = (req, res) => {
    const body = req.body

    if (!body) {
        return res.status(400).json({
            success: false,
            error: 'You must provide a person',
        })
    }

    const client = new Client(body)

    if (!client) {
        return res.status(400).json({ success: false, error: err })
    }

    client
        .save()
        .then(() => {
            return res.status(201).json({
                success: true,
                id: user._id,
                message: 'Client created!',

            })
        })
        .catch(error => {
            return res.status(400).json({
                error,
                message: 'Client not created!',
            })
        })
}

deleteClient = async (req, res) => {


    await Client.findOneAndDelete({ _id: req.params.clientId }, (err, user) => {
        if (err) {
            return res.status(400).json({ success: false, error: err })
        }

        if (!user) {
            return res
                .status(404)
                .json({ success: false, error: `Client not found` })
        }

        return res.status(200).json({ success: true, message: "Client has been deleted!" })
    }).catch(err => console.log(err))
}

getClientByclientId = async (req, res) => {
    await Client.findOne({ _id: req.params.clientId }, (err, client) => {
        if (err) {
            return res.status(400).json({ success: false, error: err })
        }

        if (!client) {
            return res
                .status(404)
                .json({ success: false, error: `User not found` })
        } 

   
        const ClientData = {
            id: client._id,
            firstname: client.firstname, 
            middleName: client.middleName, 
            lastName: client.lastName, 
            email: client.email,
            createdAt: client.createdAt,
            updatedAt: client.updatedAt,
        }


        return res.status(200).json({ success: true, data: ClientData })
    }).catch(err => console.log(err))
}

signUp = async (req, res) => {

    const password = req.body.password
    const salt = await bcrypt.genSalt(10);

    const newClient = {
 
        firstName: req.body.firstName,
        middleName: req.body.middleName,
        lastName: req.body.lastName,
        provider: req.body.provider,
        email: req.body.email,
        password: await bcrypt.hash(password, salt),
    }

    try {
        //find the user in our database 
        let clientResult = await Client.findOne({ email: req.body.email })

        if (clientResult) {
          //If user present in our database.
          return res.status(400).send({status: 'exist', message: "Client already exists"})
        } else {
          // if user is not preset in our database save user data to database.
          const clientCreated = await Client.create(newClient)
          return res.status(200).send({status: 'success', data: clientCreated, message: "Client created"})
        }
      } catch (err) {
        console.error(err)
      }
}

login = async (req, res) => {

    const email = req.body.email
    const password = req.body.password
    const user = { name: email, password }
    const salt = await bcrypt.genSalt(10);

    const clientResult = await Client.findOne({ email: req.body.email });

    if (clientResult) {
      // check user password with hashed password stored in the database
      const validPassword = await bcrypt.compare(password, userResult.password);
      if (validPassword) {
        const accessToken = AuthServices.generateAccessToken(user)
        const refreshTokenResult = jwt.sign(user, process.env.REFRESH_TOKEN_SECRET)
        return res.status(200).json({ status: "success", accessToken: accessToken, refreshToken: refreshTokenResult})

      } else {
        return res.status(400).json({ error: "Invalid Password" });
      }
    } else {
      return res.status(401).json({ error: "Client does not exist" });
    }
}

updateClientByclientId = async (req, res) => {
    const body = req.body
    const password = req.body.password
    const salt = await bcrypt.genSalt(10);
    const passwordHash = await bcrypt.hash(password, salt)

    if (!body) {
        return res.status(400).json({
            success: false,
            error: 'You must provide a body to update',
        })
    }

    console.log('name to change', req.body.name)

    const clientData = {
 
        firstName: req.body.firstName,
        middleName: req.body.middleName,
        lastName: req.body.lastName,
        provider: req.body.provider,
        email: req.body.email,
        password: await bcrypt.hash(password, salt),
    }
    
    Client.updateOne({_id: req.params.clientId}, clientData).then(
        () => {
          res.status(201).json({
            message: 'Thing updated successfully!'
          });
        }
      ).catch(
        (error) => {
            res.status(400).json({
              error: error
            });
          }
      );


}

addClientAddress = async (req, res) => {
    const body = req.body

    if (!body) {
        return res.status(400).json({
            success: false,
            error: 'You must provide a person',
        })
    }

    const clientAddress = {
        clientId: req.params.userId,
        unit: req.body.unit,
        street: req.body.street,
        city: req.body.city,
        country: req.body.country,

    }

    const address = new ClientAddress(clientAddress)

    if (!address) {
        return res.status(400).json({ success: false, error: err })
    }

    address
        .save()
        .then(() => {
            return res.status(201).json({
                success: true,
                id: user._id,
                message: 'Client Address created!',

            })
        })
        .catch(error => {
            return res.status(400).json({
                error,
                message: 'Client Address not created!',
            })
        })
}



updateAddressByclientId = async (req, res) => {
    const body = req.body

    if (!body) {
        return res.status(400).json({
            success: false,
            error: 'You must provide a body to update',
        })
    }

    const clientAddress = {
        clientId: req.params.userId,
        unit: req.body.unit,
        street: req.body.street,
        city: req.body.city,
        country: req.body.country,

    }
   
    ClientAddress.updateOne({userId: req.params.clientId}, clientAddress).then(
        () => {
          res.status(201).json({
                message: 'Address updated successfully!'
           });
        }
    ).catch(
            (error) => {
                res.status(400).json({
                error: error
                });
            }
    );
    

}


module.exports = {
    createUsers,
    getClientByclientId,
    updateAddressByclientId,
    addClientAddress,
    signUp,
    updateClientByclientId,
    deleteClient,
    token,
    login,
}