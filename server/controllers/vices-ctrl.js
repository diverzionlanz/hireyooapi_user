const Vices = require('../models/vices-model')
const bcrypt = require("bcrypt");
const mongoose = require('mongoose')
require('dotenv').config()

createVices = (req, res) => {
    const body = req.body

    if (!body) {
        return res.status(400).json({
            success: false,
            error: 'You must provide a person',
        })
    }



    console.log(Object.keys(req.body).length)
    for(var i = 0; i < Object.keys(req.body).length; i++) {
            // var month = req.body[i].month;
            // var year = req.body[i].year;
            // var monthYear = month + year;
            console.log(req.body[i].viceStatement)

            //
            
            const vices = new Vices({
                userId: req.params.userId,
                viceStatement: req.body[i].viceStatement,
            })

            if (!vices) {
                return res.status(400).json({ success: false, error: err })
            }

            vices
                .save()
                .then(() => {
                    return res.status(201).json({
                        success: true,
                        id: vices._id,
                        message: 'Vices created!',
                    })
                })
                .catch(error => {
                    return res.status(400).json({
                        error,
                        message: 'vices not created!',
                    })
                })
            //
    }

}

create4Vices = (req, res) => {
    const body = req.body

    if (!body) {
        return res.status(400).json({
            success: false,
            error: 'You must provide a person',
        })
    }

    const vices = new Vices(body)

    if (!vices) {
        return res.status(400).json({ success: false, error: err })
    }

    vices
        .save()
        .then(() => {
            return res.status(201).json({
                success: true,
                id: vices._id,
                message: 'Vices created!',
            })
        })
        .catch(error => {
            return res.status(400).json({
                error,
                message: 'vices not created!',
            })
        })
}

getAllVicesByUserId = async (req, res) => {

    await Vices.find({ userId: req.params.id }, (err, vices) => {
        if (err) {
            return res.status(400).json({ success: false, error: err })
        }
        if (!vices.length) {
            return res
                .status(404)
                .json({ success: false, error: `Vices not found` })
        }
        return res.status(200).json({ success: true, data: vices })
    }).catch(err => console.log(err))
}


getVicesById = async (req, res) => {
    await Vices.findOne({ _id: req.params.id }, (err, vices) => {
        if (err) {
            return res.status(400).json({ success: false, error: err })
        }

        if (!vices) {
            return res
                .status(404)
                .json({ success: false, error: `User not found` })
        }
        const vicesData = {
            id: vices._id,
            vicesStatement: vices.viceStatement, 
            userId: vices.userId,
            createdAt: vices.createdAt,
            updatedAt: vices.updatedAt,
        }
        return res.status(200).json({ success: true, data: vicesData })
    }).catch(err => console.log(err))
}

deleteVicesById = async (req, res) => {
    const userId = req.body.id
    await Vices.findOneAndDelete({ _id: req.body.id  }, (err, vices) => {
        if (err) {
            return res.status(400).json({ success: false, error: err })
        }

        if (!vices) {
            return res
                .status(404)
                .json({ success: false, error: `Vices not found` })
        }

        return res.status(200).json({ success: true, message: "Vices has been deleted!" })
    }).catch(err => console.log(err))
}


updateVicesById = async (req, res) => {
    const body = req.body

    if (!body) {
        return res.status(400).json({
            success: false,
            error: 'You must provide a body to update',
        })
    }

    const vicesData = {
        viceStatement: req.body.viceStatement
    }
    
    Vices.updateOne({_id: req.body.id}, vicesData).then(
        () => {
          res.status(201).json({
            message: 'Vices updated successfully!'
          });
        }
      ).catch(
        (error) => {
            res.status(400).json({
              error: error
            });
          }
      );

}


updateVicesByUserId = async (req, res) => {
    const body = req.body

    if (!body) {
        return res.status(400).json({
            success: false,
            error: 'You must provide a body to update',
        })
    }
    
    console.log(Object.keys(req.body).length)
    for(var i = 0; i < Object.keys(req.body).length; i++) {
        
        console.log(req.body[i].viceStatement)
        const vicesData = {
           viceStatement: req.body[i].viceStatement
        }
        Vices.updateOne({_id: req.body[i].id}, vicesData).then(
            () => {
            res.status(201).json({
                message: 'Vices updated successfully!'
            });
            }
        ).catch(
            (error) => {
                res.status(400).json({
                error: error
                });
            }
        );
    }

    // for (var key in req.body) {
    //     if (req.body.viceStatement(key)) {
    //       let value = req.body[key];
    //       console.log( `value for ${key} is ${value}` )
    //     }
    //   }

    // if (!body) {
    //     return res.status(400).json({
    //         success: false,
    //         error: 'You must provide a body to update',
    //     })
    // }

    // const vicesData = {
    //     viceStatement: req.body.viceStatement
    // }
    
    // Vices.updateOne({_id: req.body.id}, vicesData).then(
    //     () => {
    //       res.status(201).json({
    //         message: 'Vices updated successfully!'
    //       });
    //     }
    //   ).catch(
    //     (error) => {
    //         res.status(400).json({
    //           error: error
    //         });
    //       }
    //   );

}

module.exports = {
    createVices,
    getAllVicesByUserId,
    getVicesById,
    deleteVicesById,
    updateVicesById,
    updateVicesByUserId
}