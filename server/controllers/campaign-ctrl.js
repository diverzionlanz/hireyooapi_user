const Campaign = require('../models/campaign-model')
const bcrypt = require("bcrypt");
const mongoose = require('mongoose')
require('dotenv').config()


createCampaign = (req, res) => {
    const body = req.body

    if (!body) {
        return res.status(400).json({
            success: false,
            error: 'You must provide a person',
        })
    }

    const campaign = new Campaign(body)

    if (!campaign) {
        return res.status(400).json({ success: false, error: err })
    }

    campaign
        .save()
        .then(() => {
            return res.status(201).json({
                success: true,
                id: campaign._id,
                message: 'Campaign created!',
            })
        })
        .catch(error => {
            return res.status(400).json({
                error,
                message: 'campaign not created!',
            })
        })
}

getAllCampaignByUserId = async (req, res) => {

    await Campaign.find({ userId: req.params.id }, (err, campaign) => {
        if (err) {
            return res.status(400).json({ success: false, error: err })
        }
        if (!campaign.length) {
            return res
                .status(404)
                .json({ success: false, error: `Campaigns not found` })
        }
        return res.status(200).json({ success: true, data: campaign })
    }).catch(err => console.log(err))
}


getCampaignById = async (req, res) => {
    await Campaign.findOne({ _id: req.params.id }, (err, campaign) => {
        if (err) {
            return res.status(400).json({ success: false, error: err })
        }

        if (!campaign) {
            return res
                .status(404)
                .json({ success: false, error: `Campaign not found` })
        }
        
        return res.status(200).json({ success: true, data: campaign })
    }).catch(err => console.log(err))
}

deleteCampaignById = async (req, res) => {
    const userId = req.body.id
    await Campaign.findOneAndDelete({ _id: req.body.id  }, (err, campaign) => {
        if (err) {
            return res.status(400).json({ success: false, error: err })
        }

        if (!campaign) {
            return res
                .status(404)
                .json({ success: false, error: `Vices not found` })
        }

        return res.status(200).json({ success: true, message: "Campaign has been deleted!" })
    }).catch(err => console.log(err))
}


updateCampaignById = async (req, res) => {
    const body = req.body

    if (!body) {
        return res.status(400).json({
            success: false,
            error: 'You must provide a body to update',
        })
    }

    const campaignData = {
        campaignName: req.body.campaignName
    }
    
    Campaign.updateOne({_id: req.body.id}, campaignData).then(
        () => {
          res.status(201).json({
            message: 'Campaign updated successfully!'
          });
        }
      ).catch(
        (error) => {
            res.status(400).json({
              error: error
            });
          }
      );

}


module.exports = {
    createCampaign,
    getAllCampaignByUserId,
    getCampaignById,
    deleteCampaignById,
    updateCampaignById
}