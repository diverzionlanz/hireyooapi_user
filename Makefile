build:
	cd server && docker build . -t hireyoo/hireyoouserapi
	# docker build . -t hireyoo/hireyoouserapi

run:
	docker run -d -p 5000:5000 hireyoo/hireyoouserapi

stop:
	docker-compose down